----
Author: Elisabete Baker
Project: Adapt Demo
----
## Welcome to my Adapt Demo
I've created this project as a way to both...

* become familiar with the Adapt framework tool
* showcase what I can do

This project will serve as a sandbox and change over time, as I learn more about the framework and continue to experiment.

### What I've done...
I started this project by downloading the original Adapt framework repo, which includes several sample courses and assosicated pages, articles, blocks and components. 

One of these courses was a demo page of Adapts "Presentation" components. I began here, by editing and rearranging the existing components. From there, I updated the linked blocks, articles and course objects.

The main changes I made were to the default color theme, updating the greys and changing the accent color to orange. I also updated the accessibility focus color from orange to neon green.

### With more time...
The topic for my demo was pulled from a Twitter "moment" I created several weeks ago. As such, I already had much of the content needed for this course. However, in order to make things work well with the component formats, I needed to customize many of the original images. This took some time! 

Had I not lost so much time to creating custom content, I would have liked to added in some of the "assessment" components. I would have also liked to have experimented with the "trickle" plug-in. 

On a final note, while I did attempt to keep the course object id's in order, these could use a bit of cleaning up. As could some of the image names.

All of that said, I'm happy with the resulting demo. It was fun to create and I gained an understanding of how the framework is put together. 


Thank you for checking it out!

Elisabete